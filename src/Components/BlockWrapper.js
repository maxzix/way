import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import ElementBlock from './ElementBlock';
import MainLoader from './MainLoader';

class BlockWrapper extends React.Component {
	constructor(props) {
    	super(props);
  	}
  	shouldComponentUpdate(nextProps){
  		if(this.props.renderingJson === false){
			return true;
  		}else if(this.props.searchingFor === nextProps.searchingFor){
  			return true;
  		}else{
  			return false;
  		}
  	}
  	renderElement(){
 		return(
  		<div className = "element-wrapper">
  		{
  			this.props.renderingJson.map(function(item, i){
	  			return(
	  				<ElementBlock elementData = { item } key = {'block-key-'+i} ref={'block-ref-'+i} />
	  			);
  			})
  		}
  		</div>
  		);
  	}
  	render(){
  		if( this.props.renderingJson ){
  			return this.renderElement();
  		}else{
  			return (
  				<MainLoader />
			);
  		}
  		
  	}
}
export default BlockWrapper;