import React, { Component } from 'react';

class MainLoader extends React.Component {
  	render(){
        return(
            <div className = 'lds-hourglass' />
        );
  	}
}
export default MainLoader;