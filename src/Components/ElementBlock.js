import React, { Component } from 'react';
import axios from 'axios';

import MainLoader from './MainLoader';

class ElementBlock extends React.Component {

	constructor(props) {
  		super(props);
  		this.state = {
  			imageBase: 'none'
  		}
  	}
  	componentWillReceiveProps(nextProps){
  		if(nextProps.elementData === this.props.elementData){
  			console.log('componentWillReceiveProps true');
  		}else{
  			this.setState({
  				imageBase : nextProps.elementData.photos[0].imageBase
  			});
  		}
  	}
  	showImageLoader(base){
  		if(base === 'none'){
  			return(
  				<MainLoader />
  			);
  		}else{
  			return(
  				<img src = { base } />
  			);
  		}
  	}
	render() {
	    return (
	    	<div className="element-block">
	    		<div className="element-name">
	    			{ this.props.elementData.name }
	    		</div>
	    		<div className="element-image">
	    			{ this.showImageLoader(this.state.imageBase) }
	    		</div>
		  	</div>
	  	);
	}
}
export default ElementBlock;
