import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import HeaderIcon from './HeaderIcon';

class Header extends React.Component {

	constructor(props) {
  	super(props);
  	this.changeHandler = this.changeHandler.bind(this);
  	this.Icons = [];
  	this.state = {
  		active: false
  	}
  }
  handleClick(div) {
    this.props.onChange(div);
  }
	changeHandler(e,div) {
		if(this.state.active){
			return;
		}else{
			this.handleClick(div);
			for (var i = 0; i < this.Icons.length; i++) {
				if(this.Icons[i] === div){
					this.Icons[i].setState({
		   				active: true
		 			});
				}else{
					this.Icons[i].setState({
		   				active: false
					});
				}
	   		}
		}
	    
	}
	render() {
    return (
    	<div className="header">
	    	<ul>
	    		<HeaderIcon SearchFor = 'bar' ref= {(icon) => {this.Icons[0] = icon }} IconName="fa-beer" onChange={this.changeHandler} />
	    		<HeaderIcon SearchFor = 'food' ref= {(icon) => {this.Icons[1] = icon }} IconName="fa-utensils" onChange={this.changeHandler} />
	    		<HeaderIcon SearchFor = 'room' ref= {(icon) => {this.Icons[2] = icon }} IconName="fa-bed" onChange={this.changeHandler} />
	    		<HeaderIcon SearchFor = 'museum' ref= {(icon) => {this.Icons[3] = icon }} IconName="fa-university" onChange={this.changeHandler} />
	    		<HeaderIcon SearchFor = 'store' ref= {(icon) => {this.Icons[4] = icon }} IconName="fa-shopping-cart" onChange={this.changeHandler} />
	    		<HeaderIcon SearchFor = 'Events' ref= {(icon) => {this.Icons[5] = icon }} IconName="fa-calendar-alt" onChange={this.changeHandler}/>
	  		</ul>
	  	</div>
  	);
	}
}
export default Header;
