import React, { Component } from 'react';


import classNames from 'classnames';

class HeaderIcon extends React.Component {
	constructor(props) {
  	super(props);
  	this.state = {active: false};
  	this.handleClick = this.handleClick.bind(this);
	}
	handleClick(e) {
		var div = this;
  	this.props.onChange(e,div);   	
	}
	render() {
		var iconClasses = classNames( this.props.className, {
      	'fas fa-3x fa-fw ': true
  	});
		var liClasses = this.state.active ? 'active' : '';
 	 	return (
  		<li className = { liClasses } onClick= { this.handleClick }>
  			<i className = { iconClasses + this.props.IconName} />
			</li>
		);
	}
}
export default HeaderIcon;