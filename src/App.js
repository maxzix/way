import React, { Component } from 'react';

import Header from './Components/Header';
import BlockWrapper from './Components/BlockWrapper';
import axios from 'axios';

import './App.css';

class App extends Component {

    constructor(props) {
        super(props);
        this.changeHandler = this.changeHandler.bind(this);
    }
    componentWillMount(){
        this.setState({
            searchingFor: false
        });
    }
    componentDidMount(){
        this.startWatch();
    }     
    componentWillUnmount(){
        navigator.geolocation.clearWatch(this.watchID);
    }
    startWatch(){
        this.watchID = navigator.geolocation.watchPosition((lastPosition) => {
            this.curLat = lastPosition.coords.latitude;
            this.curLon = lastPosition.coords.longitude;
        });
    }
    changeHandler(div) {
        var search = div.props.SearchFor;
        if(search === this.state.searchingFor || this.state[search]){
            this.setState({
                searchingFor: search
            });
        }else{
            this.setState({
                searchingFor: search,
                [search] : false 
            });
            this.refs.headerWrapper.setState({
                active: true
            });
            axios('http://localhost/way_server/api/',{
                auth: {
                    username: 'admin',
                    password: 'admin'
                },
                params : {
                    lat: this.curLat,
                    lng: this.curLon,
                    type: search
                }
            })
            .then(function (response) {
                this.refs.headerWrapper.setState({
                    active: false
                });
                response.data.results.forEach(function(item,i){
                    if(item.photos){
                        axios('http://localhost/way_server/api/photo',{
                            auth: {
                                username: 'admin',
                                password: 'admin'
                            },
                            params : {
                                photo_ref: item.photos[0].photo_reference
                            }
                        })
                        .then(function (response) {
                            var base = response.data;
                            item.photos[0].imageBase = base;
                            if(this.refs.blockWrapper.refs['block-ref-'+i]){
                                this.refs.blockWrapper.refs['block-ref-'+i].setState({
                                    imageBase: base
                                });
                            }else{
                                return;
                            }
                            
                        }.bind(this));
                    }else{
                        item.photos = [{imageBase:'none'}];
                    }
                }.bind(this));
                this.setState({ 
                    [search] : response.data.results 
                });
            }.bind(this))
            .catch(function (error) {
                this.refs.headerWrapper.setState({
                    active: false
                });
            }.bind(this));
        }
    }
    render() {
        return (
            <div className="App">
                <Header ref = "headerWrapper" onChange={this.changeHandler} />
                <BlockWrapper ref = "blockWrapper" renderingThis = {this.state.searchingFor} renderingJson = {this.state[this.state.searchingFor]} />
            </div>
        );
    }
}

export default App;
